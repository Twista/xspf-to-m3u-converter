<?php

/**
 *  xspf to m3u playlist converter
 *  @author  Michal Haták <me@twista.cz>
 */

namespace Twista;

class XSPFConverter {

	/** @var string*/
	protected $src;

	/** @var array[] */
	protected $tracks = array();

	public function __construct($src){
		$this->src = $this->processXSPF($src);
	}

	/**
	 * load XSPF playlist from file
	 * @param  string $file filename
	 * @return Twista\XSPConverter
	 */
	static public function fromFile($file){
		if(!is_readable($file))
			throw new Exception("File {$file} doesn't exists");

		return new self(file_get_contents($file));
	}

	/**
	 * parse XSPF playlist
	 * @param  string $xml
	 */
	private function processXSPF($xml){
		$xml = new \SimpleXMLElement($xml);
		foreach ($xml->trackList->track as $track) {
			$this->tracks[] = array(
				'location' => (string)$track->location,
				'title' => (string)$track->title
				);
		}

	}

	/**
	 * encode loaded XSPF playlist into m3u format
	 * @return string m3u playlist
	 */
	public function toM3u(){
		$out = '#EXTM3U' . PHP_EOL;
		$counter = 1;
		foreach ($this->tracks as $track) {
			$out .= '#EXTINF:0,' . $track['title'] . PHP_EOL . $track['location'] . PHP_EOL;
		}
		return $out;
	}


}
